# Exercice 0 - commandes de base

Cet exercice permet de revoir les bases essentielles au bon fonctionnement d'un développeur sur git dans un projet.

## Déroulement
### Configurer son poste de travail

Configurez votre poste si ce n'est pas déjà fait pour travailler sur le gitlab sur lequel est hébergé ce projet.

### Récupérer un projet sur son espace personnel

Créez un fork de ce projet : Vous n'êtes pas invités à le faire en ligne de commande. 

Téléchargez ensuite ce projet en local via git.


### Creation de branches

Créez la branche : je-sais-faire-une-branche

Déposer la sur le dépot distant personnel

### Ajout de fichiers

Dans cette branche : Ajoutez votre nom/idep a la liste de participants a la formation dans le tableau Participants.md présent à la racine. 

Puis commitez sous le nom : "add name in Participants.md"

Demandez a un collègue qui en est à la même étape de proposer une pull request sur votre projet. (cela créera un conflit, le résoudre)

Enfin la derniere personne proposera directement une pull request au projet central pour 


## Validation

- Verification que c'est un bien un projet forké

- Verification que ce projet a déjà été téléchargé par git clone

- Verification que la branche "je-sais-faire-une-branche" existe.

- Verification qu'un commit "add name in Participants.md" existe dans votre dépot.
