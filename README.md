# formation-git

Dans une tentative de gamification, j'ai proposé différents exemples illustrés d'utilisation de git. 

Des solutions seront disponibles assez vite.

Veuillez forker le projet avant tout

Le schéma classique d'un exercice est le suivant

## Déroulement

[Spécification de ce qui est a faire]
## Validation

[Ce qui est vérifié par intégration continue]

## Badge de Validation
Il y a un score en haut du projet correspondant au pourcentage de validation des exercices.

[![coverage report](https://gitlab.com/antoine-brunetti/formation-git/badges/master/coverage.svg)](https://gitlab.com/antoine-brunetti/formation-git/-/commits/master)

S'il est undefined me contacter.