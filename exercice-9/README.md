# Recherche de fichiers dans l'historique

Vous voulez sortir le code de votre dépot de l'insee, mais vous savez qu'au tout début du projet quelqu'un s'est amusé a mettre un mot de passe en clair dans le projet. Par chance vous vous rappelez de la forme du mot de passe : il se retrouve entre deux balise html :
```html
<p hidden> </p>
```

Saurez vous le retrouver ? 

## Aide


## Validation

Pour valider cet exercice il faudra rajouter en variable d'environnement du projet forké, le mot de passe sous 'SECRET_PASSWORD'.
