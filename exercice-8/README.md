# Exercice 8 - commandes utiles

Parfois, certaines commandes peuvent faire gagner beaucoup de temps dans la réalisation de certaines tâches / savoir où l'on se trouve.

## Déroulement

### Différences 

Copiez le contenu du fichier saved-data/data-after.json dans data.json 

Consultez les différences et ajoutez le fruit qui a changé dans la variable "SECRET_FRUIT".

