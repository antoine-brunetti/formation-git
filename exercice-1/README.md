# Création d'un historique cohérent

Vous et votre petite équipe de développeur commencent à avoir roulé leur bosse sur ce grand projet aux commits si nombreux. Vous décidez de mettre un peu d'ordre dans ce projet 

## Déroulement

Regrouper les commits inutiles en un seul du nom de votre choix

## Aide
<details><summary>Aide</summary>
<p>

</p>
</details>

## Validation
Il faut qu'en définitive, il y ait moins de 20 commits sur le projet : le pipeline le verifiera


