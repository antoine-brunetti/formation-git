# Creation d'un nouveau projet

Vous désirez vous lancer a l'aventure avec vos chaines SAS qui ont su traverser les âges des maintenances (on passera sur certains détails :) ), et enfin aborder la question d'un versionning plus poussé que le gestionnaire de fichiers de l'OS d'AUS ? Créez donc un nouveau projet.

## Déroulement

A partir d'un code existant / dossier existant, créez un dépot git et pushez le. 

Puis commitez : "init commit"

## Validation

Entrez votre url ici : [Url du projet](url) (voir markdown)