# Exercice 4 - Travail sur 2 remotes 

Votre équipe projet est décomposée en 2 puisqu'une prestation est en cours.. Le fonctionnement projet est un peu dégradé puisque 2 dépots sont a disposition pour la même application, vous devez vous assurer que vous êtes a jour sur les dépots des 2 cotés.

## Déroulement



A la surprise générale, le prestataire a intégrer des changements sur des fichiers que vous avez touché : 

Changez des fichiers sur le nouveau dépot créé et également le distant

Puis récupérez les changement distants du prestataires sans conflits en privilégiant directement ses ajouts sur les vôtres.
