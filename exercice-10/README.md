# Exercice 10

Cet exercice est un exercice sur la gestion de dépot. Vous êtes sur une application aux évolutions importantes et significative et avez besoin de garder une trace du passé (formulaire d'enquêtes / Editions), pour cela vous maintenez des jar dans un répertoire sur le côté. Mais le volume commence a se faire ressentir, et les différentes migrations sont lourdes.. Utilisez des outils natifs a git pour marquer les version du projet que vous voulez voir conservées pour ensuite pouvoir rebuilder plus tard.

##
