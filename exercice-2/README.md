# Exercice 2

Vous avez commencé a travailler sur une tâche et vous avez commit en local sur master, or, un bug de production vient d'arriver
( Tous les coups sont permis ex : cherry pick | branching)

Ensuite le bug déclaré est le suivant : 

```
Lorsque j'accède au projet sur gitlab, 
j'ai un problème d'accès au dossier de documentation sur exercice-2/.documentation sur windows,
Tu pourrais renommer ce dossier en /documentation puis cocher le README quand c'est fait ?  
```

## Prérequis : 

Créez un commit en local sur la branche master, en cochant la case à cocher suivante 

- [ ] commit prerequis exercice 2

## Déroulement 
1) Renommer le dossier après avoir pu déplacer son commit

2) Cocher la case ci dessous et commit .
- [ ] commit exercice 2

## Aide
<details><summary>Aide</summary>
<p>

</p>
</details>

## Validation

Un scan va se faire sur ce Readme.md attestant qu'il est bien pas coché pour le commit prerequis, et qu'il est bien coché pour le second (pour cocher ajouter un X) 
et que le dossier /.documentation est maintenant un dossier /documentation

